using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using UnityEngine;
using UnityEngine.UI;

public class Message {
    public string UserName { get; set; }
    public string Text { get; set; }
}


public class Command {
    public string type;
    public string data;
}


public class MainPage : MonoBehaviour {

    public Text ReceivedText;
    public InputField inputField;
    public Button SendButton;
    public Button ReconnectButton;
    public Button DisconnectButton;
    private IConnector connector;
    private string currentText;
    
    public async Task Start() {

        //connector = new SignalRNS.SignalRConnector();
        //connector = new WebSocketSharpNS.WebSocketSharpConnector();
        connector = new NativeWebSocketNS.NativeWebSocketConnector();

        connector.OnMessageReceived += UpdateReceivedMessages;
        connector.OnAdminMessageReceived += UpdateAdminReceivedMessages;
        await connector.InitAsync();
        SendButton.onClick.AddListener(SendMessage);
        ReconnectButton.onClick.AddListener(Reconnect);
        DisconnectButton.onClick.AddListener(Disconnect);
        
    }


	public void Update() {
        connector.DispatchMessageQueue();
    }


	private void UpdateReceivedMessages(Message newMessage) {

        try {
            var lastMessages = this.ReceivedText.text;
            if (string.IsNullOrEmpty(lastMessages) == false) {
                lastMessages += "\n";
            }
            lastMessages += $"<{newMessage.UserName}> {newMessage.Text}";
            this.ReceivedText.text = lastMessages;
        } catch(Exception ex) {
            UnityEngine.Debug.LogError(ex.Message);
        }
    }


    private void UpdateAdminReceivedMessages(string message) {

        try { 
            var lastMessages = this.ReceivedText.text;
            if (string.IsNullOrEmpty(lastMessages) == false) {
                lastMessages += "\n";
            }
            lastMessages += $"ADMIN: {message}";
            this.ReceivedText.text = lastMessages;
        } catch(Exception ex) {
            UnityEngine.Debug.LogError(ex.Message);
        }
    }


    private async void SendMessage() {

        await connector.SendMessageAsync(new Message {
                                            UserName = "ryan",
                                            Text = inputField.text
                                        });

        inputField.Select();
        inputField.text = "";
    }


    private async void Reconnect() {
        await connector.ReconnectAsync();
    }


    private async void Disconnect() {
        await connector.DisconnectAsync();
    }
}
