using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NativeWebSocket;

using System;
using System.Threading.Tasks;

using Newtonsoft.Json;


namespace NativeWebSocketNS {


    public class NativeWebSocketConnector : IConnector {

        private Action<Message> _OnMessageReceived;

        public Action<Message> OnMessageReceived {
            get => _OnMessageReceived;
            set => _OnMessageReceived = value;
        }

        private Action<string> _OnAdminMessageReceived;

        public Action<string> OnAdminMessageReceived {
            get => _OnAdminMessageReceived;
            set => _OnAdminMessageReceived = value;
        }

        private WebSocket connection;


        public async Task InitAsync() {
            try {
                connection = new WebSocket("ws://localhost:5000/chat");

                connection.OnMessage += (bytes) => {
                    UnityEngine.Debug.LogError("getting a message");
                    var body = System.Text.Encoding.UTF8.GetString(bytes);
                    Command command = JsonConvert.DeserializeObject<Command>(body);
                    if (command.type.Equals("ReceiveMessage")) {


                        Message msg = JsonConvert.DeserializeObject<Message>(command.data);
                        OnMessageReceived?.Invoke(msg);
                    } else if (command.type.Equals("AdminMessage")) {

                        string msg = command.data;
                        OnAdminMessageReceived?.Invoke(msg);
                    } else {
                        UnityEngine.Debug.LogError("Unknown command from websocket");
                    }
                };

                connection.OnError += (error) => {
                    UnityEngine.Debug.LogError(error);
                };
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
            await StartConnectionAsync();
       }

        public Task SendMessageAsync(Message message) {
            try {
                string jsonString = JsonConvert.SerializeObject(message);
                if (connection.State == WebSocketState.Open) {
                    connection.SendText(jsonString);
                } else {
                    throw new Exception("Websocket connection not open");
                }

            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
            return Task.FromResult(0);
        }


        private Task StartConnectionAsync() {
            try {
                if (connection.State == WebSocketState.Closed) {
                    connection.Connect();
                } else {
                    throw new Exception("Can't start websocket connection when not closed");
                }

            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
            return Task.FromResult(0);
        }


        public async Task ReconnectAsync() {
            await StartConnectionAsync();
        }


        public Task DisconnectAsync() {
            try {
                if (connection.State == WebSocketState.Open) {
                    connection.Close();
                } else {
                    throw new Exception("Trying to close a non-open websocket connection");
                }
            } catch (Exception ex) {
                UnityEngine.Debug.LogError($"Error {ex.Message}");
            }
            return Task.FromResult(0);
        }


        public void DispatchMessageQueue() {
            connection.DispatchMessageQueue();
        }

    }

}