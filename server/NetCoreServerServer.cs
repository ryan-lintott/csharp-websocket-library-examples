using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using NetCoreServer;
using Newtonsoft.Json;


namespace NetCoreServerChatNS {

    public class Command {
        public string type;
        public string data;
    }

    class ChatSession : WsSession {
        public ChatSession(WsServer server) : base(server) {}

        public override void OnWsConnected(HttpRequest request) {
            
            Console.WriteLine($"Chat WebSocket session with Id {Id} connected!");
            // Send invite message
            Command command = new Command();
            command.type = "AdminMessage";
            command.data = $"{Id} connected";
            ((WsServer)Server).MulticastText(JsonConvert.SerializeObject(command));
        }

        public override void OnWsDisconnected() {
           
            Console.WriteLine($"Chat WebSocket session with Id {Id} disconnected!");
            Command command = new Command();
            command.type = "AdminMessage";
            command.data = $"{Id} disconnected";
            ((WsServer)Server).MulticastText(JsonConvert.SerializeObject(command));
        }

        public override void OnWsReceived(byte[] buffer, long offset, long size) {
            
            string message = Encoding.UTF8.GetString(buffer, (int)offset, (int)size);
            Console.WriteLine("Incoming: " + message);
            Command command = new Command();
            command.type = "ReceiveMessage";
            command.data = message;
            // Multicast message to all connected sessions
            ((WsServer)Server).MulticastText(JsonConvert.SerializeObject(command));
        }

        protected override void OnError(SocketError error) {
            Console.WriteLine($"Chat WebSocket session caught an error with code {error}");
        }
    }

    class ChatServer : WsServer {
        public ChatServer(IPAddress address, int port) : base(address, port) {}
        protected override TcpSession CreateSession() { return new ChatSession(this); }
        protected override void OnError(SocketError error) {
            Console.WriteLine($"Chat WebSocket server caught an error with code {error}");
        }
    }
}