using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;

namespace WebSocketSharpChatNS {

    public class Command {
        public string type;
        public string data;
    }
    public class WebSocketSharpChat : WebSocketBehavior {
        public WebSocketSharpChat() {
        }

        protected override void OnClose (CloseEventArgs e) {
            
            Console.WriteLine($"Chat WebSocket session with Id {ID} disconnected!");
            Command command = new Command();
            command.type = "AdminMessage";
            command.data = $"{ID} disconnected";

            Sessions.Broadcast(JsonConvert.SerializeObject(command));
        }

        protected override void OnMessage (MessageEventArgs e) {
            //var msg = String.Format (fmt, _name, e.Data);
            Console.WriteLine($"Receiving message: {e.Data}");
            Command command = new Command();
            command.type = "ReceiveMessage";
            command.data = e.Data;

            Sessions.Broadcast(JsonConvert.SerializeObject(command));
        }
 
        protected override void OnOpen () {

            Console.WriteLine($"Chat WebSocket session with Id {ID} connected!");
            Command command = new Command();
            command.type = "AdminMessage";
            command.data = $"{ID} connected";

            Sessions.Broadcast(JsonConvert.SerializeObject(command));
        }

        protected override void OnError (WebSocketSharp.ErrorEventArgs e) {
            Console.WriteLine($"ERROR: {e.Message}");
        }
    }
}