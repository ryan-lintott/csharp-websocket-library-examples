﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

using System.Net;


namespace ChatSample {
    public class Program {
        public static int Main(string[] args) {
            if(args.Length == 0) {
                System.Console.WriteLine("Invalid argument");
                return 1;
            }
            int returnVal = 0;

            if(args[0].Equals("signalr")) {

                CreateHostBuilder(args).Build().Run();

            } else if(args[0].Equals("netcoreserver")) {
                
                var server = new NetCoreServerChatNS.ChatServer(IPAddress.Any, 5000);
                server.Start();
                Console.WriteLine("\nStarting netcoreserver server, press Enter key to stop the server...");
                Console.ReadLine();
                server.Stop();

            } else if(args[0].Equals("websocket-sharp")) {

                var wssv = new WebSocketSharp.Server.WebSocketServer(5000);
                wssv.AddWebSocketService<WebSocketSharpChatNS.WebSocketSharpChat>("/chat");
                wssv.Start();
                Console.WriteLine("\nStarting websocket-sharp server, press Enter key to stop the server...");
                Console.ReadLine();
                wssv.Stop();

            } else {
                System.Console.WriteLine("Invalid argument");
                returnVal = 1;
            }

            return returnVal;
        }

   
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                });
    }
}