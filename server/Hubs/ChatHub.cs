using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace ChatSample.Hubs {
    public class ChatHub : Hub {
        public async Task Send(string user, string message) {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
        public async override Task OnConnectedAsync() {
            await Clients.All.SendAsync("AdminMessage", $"{Context.ConnectionId} connected");
            await base.OnConnectedAsync();
        }
        public async override Task OnDisconnectedAsync(Exception? ex) {
            await Clients.All.SendAsync("AdminMessage", $"{Context.ConnectionId} disconnected");
            await base.OnDisconnectedAsync(ex);
        }
    }
}







